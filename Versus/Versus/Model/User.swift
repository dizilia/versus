//
//  User.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/15/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation

class User {

    var id           :String
    var uid          :String
    var name         :String
    var avatar       :String?
    var ownQuestions :[String]
    
    var dictionary: [String: Any] {
        return [
            "name"        : name,
            "ownQuestions": ownQuestions,
            "uid"         : uid,
            "avatar"      : avatar
        ]
    }
    
    init(id: String, uid: String, name: String, avatar: String, ownQuestions: [String]) {
        
        self.id           = id
        self.uid          = uid
        self.name         = name
        self.ownQuestions = ownQuestions
        self.avatar       = avatar.isEmpty ? "3FI7AEH0VxU.jpg" : avatar
    }
}

class AnonimUser: User {

    init() {

        super.init(id: "", uid: "", name: "Anonim", avatar: "", ownQuestions: [])
    }
}

extension User{
    convenience init?(dictionary: [String : Any], id: String) {
        guard let name         = dictionary["name"] as? String,
              let uid          = dictionary["uid"] as? String,
              let ownQuestions = dictionary["ownQuestions"] as? [String],
              let avatar       = dictionary["avatar"] as? String?

        else {

            return nil
        }

        self.init(id: id, uid: uid, name: name, avatar: avatar ?? "", ownQuestions: ownQuestions)
    }
}
