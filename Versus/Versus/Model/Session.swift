//
//  Session.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/10/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation
import RxSwift


class Session {
    
    public let dataProvider = DataProvider()
    
    public var user: User?
    public var tags :Variable<[DbTag]> = Variable([])

    init() {
        
        dataProvider.didUpdateUser = { [weak self] user in

            self?.user = user
        }
        
        dataProvider.didUpdateTags = { [weak self] tags in
            
            self?.tags.value = tags
        }
    }
}
