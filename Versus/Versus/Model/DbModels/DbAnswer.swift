//
//  DbAnswer.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/15/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation

struct DbAnswer {

    var id        :String
    var answer    : Int
    var questionId: String

    var dictionary: [String: Any] {
        return [
            "answer"      : answer,
            "questionId"  : questionId,
        ]
    }
}

extension DbAnswer {

    init?(dictionary: [String : Any], id: String) {
        guard let answer   = dictionary["answer"] as? Int,
            let questionId = dictionary["questionId"] as? String
            
            else {
                
                return nil
        }
        
        self.init(id: id, answer: answer, questionId: questionId)
    }
}
