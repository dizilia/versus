//
//  Question.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/14/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation

struct DbQuestion {
    
    var id            : String
    var createdAt     : Double
    var createdBy     : String
    var creatorAvatar : String
    var creatorName   : String
    var title         : String
    var description   : String
    var voteOption1   : Int
    var voteOption2   : Int
    var isVertical    : Bool
    var image1name    : String
    var image2name    : String
    var tags          : String

    var dictionary: [String: Any] {
        return [
            "title"         : title,
            "description"   : description,
            "voteOption1"   : voteOption1,
            "voteOption2"   : voteOption2,
            "isVertical"    : isVertical,
            "image1name"    : image1name,
            "image2name"    : image2name,
            "createdBy"     : createdBy,
            "createdAt"     : createdAt,
            "creatorAvatar" : creatorAvatar,
            "creatorName"   : creatorName,
            "tags"          : tags
            
        ]
    }
    
    func creationDate() -> Date {

        return Date(timeIntervalSince1970: createdAt)
    }
}

extension DbQuestion{
    init?(dictionary: [String : Any], id: String) {
        guard   let title = dictionary["title"] as? String,
                let description   = dictionary["description"] as? String,
                let voteOption1   = dictionary["voteOption1"] as? Int,
                let voteOption2   = dictionary["voteOption2"] as? Int,
                let isVertical    = dictionary["isVertical"]  as? Bool,
                let image1name    = dictionary["image1name"]  as? String,
                let image2name    = dictionary["image2name"]  as? String,
                let createdAt     = dictionary["createdAt"]   as? Double,
                let createdBy     = dictionary["createdBy"]   as? String,
                let creatorAvatar = dictionary["creatorAvatar"]  as? String,
                let creatorName   = dictionary["creatorName"]  as? String,
                let tags          = dictionary["tags"] as? String
            else {
                return nil
                
        }

        self.init(id: id, createdAt: createdAt, createdBy: createdBy, creatorAvatar: creatorAvatar, creatorName: creatorName, title: title, description: description, voteOption1: voteOption1, voteOption2: voteOption2, isVertical: isVertical, image1name: image1name, image2name: image2name, tags: tags)
    }
}
