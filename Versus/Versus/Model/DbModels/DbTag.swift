//
//  DbTag.swift
//  Versus
//
//  Created by Eugene Liashenko on 6/21/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation

struct DbTag: Hashable {
    
    var id   : String
    var name : String
    
    var dictionary: [String: Any] {
        return [
            "name" : name
        ]
    }
}

extension DbTag {
    
    init?(dictionary: [String : Any], id: String) {
        guard let name   = dictionary["name"] as? String
            else {
                
                return nil
        }

        self.init(id: id, name: name)
    }
}
