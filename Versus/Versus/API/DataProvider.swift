//
//  DataProvider.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/15/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation
import Firebase
import RxSwift

class DataProvider {
    
    public var didUpdateUser     : ((User)->Void)?
    public var didUpdateQuestions: (([DbQuestion])->Void)?
    public var didUpdateTags     : (([DbTag])->Void)?

    private var documents     : [DocumentSnapshot] = []
    
    public  var storage           = StorageDataProvider()
    private var userDataProvider = UserDataProvider()
    private var db: Firestore!

    private let disposeBag = DisposeBag()

    private var questionsListener : ListenerRegistration!
    
    fileprivate func baseQuestionsQuery() -> Query {
        return db.collection("Questions").limit(to: 500)
    }
    
    fileprivate var questionsQuery: Query? {
        didSet {
            if let listener = questionsListener {
                listener.remove()
            }
        }
    }

    private var tagsListener      : ListenerRegistration!
    
    fileprivate func baseTagssQuery() -> Query {
        return db.collection("Tags").limit(to: 500)
    }

    fileprivate var tagssQuery: Query? {
        didSet {
            if let listener = tagsListener {
                listener.remove()
            }
        }
    }
    
    //-----
    
    private var questions : [DbQuestion] = []
    
    deinit {

        questionsListener.remove()
        tagsListener.remove()
    }
    
    //-----
    init() {
        
        db = Firestore.firestore()
    }
    
    public func startListeners() {
        
        userDataProvider.didUpdateUser = {[weak self] user in

            self?.didUpdateUser?(user)

            self?.setupQuestionsListener()
        }

        userDataProvider.startUsersListener()
        
        startTagsListening()
    }

    public var currentUser :User {
        get {

            return self.userDataProvider.currentUser
        }
    }

    public func getImage(named: String, completion:((UIImage?)->Void)?) {

        storage.getImage(named: named, completion: completion)
    }
    
    private func setupQuestionsListener() {
        
        questionsQuery = baseQuestionsQuery()
        
        questionsListener = questionsQuery?.addSnapshotListener { (documents, error) in
            guard let snapshot = documents else {
                print("Error fetching questions results: \(error!)")
                return
            }
            
            let results = snapshot.documents.map { (document) -> DbQuestion in
                if let question = DbQuestion(dictionary: document.data(), id: document.documentID) {
                    return question
                } else {
                    fatalError("Unable to initialize type \(DbQuestion.self) with dictionary \(document.data())")
                }
            }
            
            self.questions = results.sorted(by: { $0.createdAt > $1.createdAt })

            self.didUpdateQuestions?(self.questions)
        }
    }
    
    private func startTagsListening() {
        
        tagssQuery = baseTagssQuery()
        
        tagsListener = tagssQuery?.addSnapshotListener({ (snapshot, error) in
            
            guard let snapshot_ = snapshot else {
                print("Error fetching tags results: \(error!)")
                return
            }
            
            let results = snapshot_.documents.map { (document) -> DbTag in
                
                if let tag = DbTag(dictionary: document.data(), id: document.documentID) {
                    return tag
                } else {
                    fatalError("Unable to initialize type \(DbTag.self) with dictionary \(document.data())")
                }
            }

            self.didUpdateTags?(results)
        })
    }
    
    public func getUsersAnswers(completion:@escaping (([DbAnswer])->Void)) {

        db.collection("Users").document(currentUser.id).collection("answeredQuestions").getDocuments { (snapshot, error) in

            guard let documents = snapshot else {
                
                print("Error fetching documents results: \(error!)")
                
                completion([])
                return
            }
            
            let answers = documents.documents.map({ document -> DbAnswer in
                
                if let answer = DbAnswer(dictionary: document.data(), id: document.documentID) {
                    
                    return answer
                    
                }else {
                    
                    fatalError("Unable to initialize type \(DbQuestion.self) with dictionary \(document.data())")
                }
            })

            completion(answers)
        }
        
    }
    
    public func voteFor(question: DbQuestion, answer: Int) {
        
        let updateQuestions: ((Bool)->Void) = { [weak self] wasAlreadyVotedByThisUser in

            guard let self_ = self else { return }

            self_.db.collection("Questions").document(question.id).updateData(
                [
                    (answer == 1 ? "voteOption1" : "voteOption2") : answer == 1
                        ? ((question.voteOption1 + 1))
                        : ((question.voteOption2 + 1)),
                    
                    (answer == 1 ? "voteOption2" : "voteOption1") : answer == 1
                        ? (wasAlreadyVotedByThisUser ? (question.voteOption2 - 1) : question.voteOption2)
                        : (wasAlreadyVotedByThisUser ? (question.voteOption1 - 1) : question.voteOption1)
                ])
            { err in
                
                if let err = err {
                    
                    print("Error updating document: \(err)")
                } else {
                    
                    print("Document successfully updated")
                }
            }
        }
        
        getUsersAnswers {[unowned self] answers in
            
            if let answer_ = answers.first(where: { (answ) -> Bool in

                return answ.questionId == question.id
            }) {
                
                self.userDataProvider.updateUser(answer: answer_, question: question, answerValue: answer, completion: {
                    
                    updateQuestions(true)
                })
            }
            else {
                
                self.userDataProvider.addUserAnswerFor(question: question, answerValue: answer, completion: {
                    
                    updateQuestions(false)
                })
            }
        }
    }
    
    public func createQuizWith(title: String, image1: UIImage, image2: UIImage, tags:[DbTag], completion:((Bool)->Void)?) {

        createQuestionWith(title: title, tags: tags) {[weak self] questionId in
            
            guard let self_ = self, let questionId_ = questionId else {
                
                completion?(false)
                
                return
            }

            let image1StorageName = questionId_ + "im1"
            let image2StorageName = questionId_ + "im2"

            let uploadImage1Ob = self_.storage.upload(image: image1, name: image1StorageName)
            let uploadImage2Ob = self_.storage.upload(image: image2, name: image2StorageName)
            
            Observable.combineLatest(uploadImage1Ob, uploadImage2Ob).subscribe(onNext: {[unowned self_] (res1, res2) in
                
                if res1 && res2 {
                    
                    self_.updateQuestionWith(questionId: questionId_, image1Name: image1StorageName, image2Name: image2StorageName, callBack: { updateResult in

                        completion?(updateResult)
                    })
                }else {

                    //TODO: Remove images and quiz

                    completion?(false)
                }
            }).disposed(by: self_.disposeBag)
        }
    }
    
    private func updateQuestionWith(questionId: String, image1Name: String, image2Name: String, callBack: ((Bool)->Void)?) {

        db.collection("Questions").document(questionId).updateData(
            [
                "image1name"   : image1Name,
                "image2name"   : image2Name,
            ])
        { err in
            
            if let err = err {
                
                print("Error updating document: \(err)")
                callBack?(false)
            } else {
            
                print("Document successfully updated")
                callBack?(true)
            }
        }
    }
    
    private func createQuestionWith(title: String, tags:[DbTag], completion:((String?)->Void)?) {

        var questionRef: DocumentReference? = nil

        let tagsString = tags.map({$0.id}).joined(separator: ",")

        questionRef = db.collection("Questions").addDocument(data: [
                                                                "description"   : "",
                                                                "isVertical"    : false,
                                                                "title"         : title,
                                                                "voteOption1"   : 0,
                                                                "voteOption2"   : 0,
                                                                "image1name"    : "",
                                                                "image2name"    : "",
                                                                "createdAt"     : Date().timeIntervalSince1970,
                                                                "createdBy"     : currentUser.id,
                                                                "creatorAvatar" : currentUser.avatar,
                                                                "creatorName"   : currentUser.name,
                                                                "tags"          : tagsString
            ]) { error in
                
                if let _ = error {
                    ///TODO: Handle error
                    completion?(nil)
                }else {

                    completion?(questionRef!.documentID)
                }
        }
    }
}
