//
//  StorageDataProvider.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/22/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation
import FirebaseStorage
import RxSwift

class StorageDataProvider {
 
    var storage: StorageReference!
    
    private var imageCache = NSCache<NSString, UIImage>()

    init() {

        storage = Storage.storage().reference()
    }
    
    public func getImage(named: String, completion:((UIImage?)->Void)?) {
        
        if let cachedImage = imageCache.object(forKey: named as NSString) {

            completion?(cachedImage)

            return
        }

        storage.child(named).getData(maxSize: 1 * 2048 * 2048) {[weak self] data, error in

            var resultImage: UIImage? = nil

            if let error_ = error {

                print("ERROR: StorageDataProvider::getImage \(error_)")
                
            } else {

                resultImage = UIImage(data: data!)

                self?.imageCache.setObject(resultImage!, forKey: named as NSString)
            }
            
            completion?(resultImage)
        }
    }

    public func upload(image: UIImage, name: String) -> Observable<Bool> {

        return Observable.create({ [unowned self] observer in

            //TODO make maximum 1 MB size
            guard let data = UIImageJPEGRepresentation(image, 0.3) else {
            
                observer.onNext(false)
                observer.onCompleted()

                return Disposables.create()
            }
            
            // Create a reference to the file you want to upload
            let imageRef = self.storage.child(name)
            
            // Upload the file to the path "images/rivers.jpg"
            let uploadTask = imageRef.putData(data, metadata: nil) {[weak self] (metadata, error) in
            
                guard let metadata = metadata else {

                    observer.onNext(false)
                    observer.onCompleted()

                    return
                }
                
                if let error_ = error {
                    
                    print("VERSUS ERROR: imageRef.putData \(error_)")
                    
                    observer.onNext(false)
                    observer.onCompleted()
                }else {
                    
                    observer.onNext(true)
                    observer.onCompleted()
                }
//                self?.storage.downloadURL { (url, error) in
//
//                    guard let downloadURL = url else {
//
//                        observer.onNext(false)
//                        observer.onCompleted()
//
//                        return
//                    }
//
//                    observer.onNext(true)
//                    observer.onCompleted()
//                }
            }
            
            return Disposables.create()
        })
    }
}
