//
//  UserDataProvider.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/24/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation
import Firebase

class UserDataProvider {
    
    public var didUpdateUser  : ((User)->Void)?
    public var currentUser    : User!
 //   private var authUser
    private var db            : Firestore!

    init() {
        
        db = Firestore.firestore()
    }
    
    public func startUsersListener() {

        getAuthUserInfo { [weak self] (uid, name) in
            
            guard let uid_ = uid else {

                self?.updateUserWith(newUser: AnonimUser())
                
                return
            }
            
            self?.getUserWith(uid: uid_, callback: {[weak self] user in
                
                if let user_ = user {

                    self?.updateUserWith(newUser: user_)

                }else {
                 
                    self?.createUserWith(uid: uid_, name: name ?? "Default Name", callBack: { createdUser in
                        
                        self?.updateUserWith(newUser: createdUser ?? AnonimUser())
                    })
                }
            })
        }
    }
    
    private func updateUserWith(newUser: User) {
        
        currentUser = newUser

        didUpdateUser?(currentUser)
    }
    
    private func getUserWith(uid: String, callback:((User?) -> Void)?) {
        
        db.collection("Users").whereField("uid", isEqualTo: uid).getDocuments { (snapshot, error) in
            
            if let document = snapshot?.documents.first {
                
                callback?(User(dictionary: document.data(), id: document.documentID))
            }else {
                
                callback?(nil)
            }
        }
    }
    
    private func createUserWith(uid: String, name: String, callBack:((User?) -> Void)?) {
        
        db.collection("Users").addDocument(data: [
            "name"         : name,
            "uid"          : uid,
            "ownQuestions" : [],
            "avatar"       : ""
        ]) {[weak self] error in
            
            if let _ = error {
                ///TODO: Handle error
                callBack?(nil)

            }else {

                self?.getUserWith(uid: uid, callback: { user in

                    callBack?(user)
                })
            }
        }
    }

    private func getAuthUserInfo(callback: ((String?, String?)->Void)?) {

        if let authUser = Auth.auth().currentUser {

            callback?(authUser.uid, authUser.displayName)

//            authUser.getIDToken { (token, error) in
//
//                if let _ = error {
//
//                    callback?(nil, nil)
//
//                }else {
//
//                    callback?(token, authUser.displayName)
//                }
//            }
        }else {
            
            callback?(nil, nil)
        }
    }
    
    public func updateUser(answer: DbAnswer, question: DbQuestion, answerValue: Int, completion:(()->Void)?) {
        
        db.collection("Users").document(currentUser.id).collection("answeredQuestions").document(answer.id).updateData([
            
            "answer": answerValue
        ]) { err in
            
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
            }
            
            completion?()
        }
    }
    
    public func addUserAnswerFor(question: DbQuestion, answerValue: Int, completion:(()->Void)?) {
        
        self.db.collection("Users").document(currentUser.id).collection("answeredQuestions").addDocument(data: [
            "answer"    : answerValue,
            "questionId": question.id
            ], completion: { err in
                
                if let err = err {
                    
                    print("Error add document: \(err)")
                    
                } else {
                    
                    print("Document successfully written!")
                }
                
                completion?()
        })
    }
}
