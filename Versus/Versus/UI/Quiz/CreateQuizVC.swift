//
//  CreateQuizVC.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/24/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit
import MBProgressHUD

class CreateQuizVC: UIViewController {

    @IBOutlet weak var titleTextView  : UITextView!
    @IBOutlet weak var leftImageView  : UIImageView!
    @IBOutlet weak var rightImageView : UIImageView!
    @IBOutlet weak var createVotingBtn: UIButton!
    @IBOutlet weak var tagsLabel      : UILabel!
    
    public var session: Session!

    private var picker:ImagePicker?
    private var tags: [DbTag] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        updateVotingButton()

        appleSkining()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        UIApplication.shared.statusBarStyle = .lightContent
    }
        
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)

    }

    @IBAction func hideKeyboard(_ sender: Any) {
        
        view.resignFirstResponders()
    }

    @IBAction func addTags(_ sender: Any) {
        
        router.quizUIRouter.showTagSelectionFrom(parentVC: self) {[weak self] tags in
            
            self?.tags = tags

            let tagsText = tags.map({ $0.name })

            self?.tagsLabel.text = tagsText.joined(separator: ", ")
        }
    }
    
    private func updateVotingButton() {
        
        createVotingBtn.isEnabled = true
    }

    @IBAction func selectLeftImage(_ sender: Any) {
        
        picker = ImagePicker(holderVC: self)
        
        picker?.selectPhoto(callback: {[weak self] image in
            
            self?.leftImageView.image = image
            
            self?.updateVotingButton()
        })
    }

    @IBAction func selectRightImage(_ sender: Any) {
        
        picker = ImagePicker(holderVC: self)
        
        picker?.selectPhoto(callback: {[weak self] image in
            
            self?.rightImageView.image = image

            self?.updateVotingButton()
        })
    }

    @IBAction func createVote(_ sender: Any) {
        
        if !validate() { return }

        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        
        session.dataProvider.createQuizWith(title: titleTextView.text, image1: leftImageView.image!, image2: rightImageView.image!, tags: tags) {[weak self] result in

            hud.hide(animated: true)

            self?.router.quizUIRouter.backFrom(quizVC: self!)
        }
    }

    @IBAction func back(_ sender: Any) {

        router.quizUIRouter.backFrom(quizVC: self)
    }
    
    private func validate() -> Bool {
        
        if titleTextView.text.isEmpty {
            
            router.show(message: "Please fill the Title field")
            
            return false
        }
        
        if leftImageView.image == nil || rightImageView.image == nil {
            
            router.show(message: "Please select 2 images")
            
            return false
        }
        
        return true
    }
    //MARK:-
    private func appleSkining() {

        navigationController?.navigationBar.barTintColor = UIColor.vePurple()
        
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = attributes
        
        titleTextView.delegate = titleTextView
        
        titleTextView.placeholder = "Enter title of your voting"
    }
}
