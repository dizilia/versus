//
//  TagsSelectionVC.swift
//  Versus
//
//  Created by Eugene Liashenko on 6/21/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit
import RxSwift

class TagsSelectionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table: UITableView!
    
    public var session               : Session!
    public var didFinishSelectionTags:(([DbTag]) -> Void)?
    
    private var tags: [DbTag] = []
    private var selectedIndexes = Set<DbTag>()
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        session.tags.asObservable().subscribe(onNext: { [weak self] tags in
            
            self?.tags = tags

            self?.table.reloadData()
            
        }).disposed(by: disposeBag)
    }
    
    @IBAction func apply(_ sender: Any) {
        
        didFinishSelectionTags?(Array(selectedIndexes))

        router.quizUIRouter.backFrom(tagsSelectionVC: self)
    }
    
    @IBAction func back(_ sender: Any) {

        router.quizUIRouter.backFrom(tagsSelectionVC: self)
    }
    
    //MARK:-
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return tags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TagCell", for: indexPath)
        
        cell.textLabel?.text = tags[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        
        if cell.accessoryType == .checkmark {
            
            cell.accessoryType = .none
            
            selectedIndexes.remove(tags[indexPath.row])

        }else {
            
            cell.accessoryType = .checkmark

            selectedIndexes.insert(tags[indexPath.row])
        }
    }
}
