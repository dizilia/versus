//
//  QuizUIRouter.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/24/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation

import UIKit

class QuizUIRouter {
    
    private weak var mainRouter :UIRouter!
    private var session         :Session!
    private let storyboard      :UIStoryboard!
    
    init (mainRouter :UIRouter, session :Session) {
        
        self.mainRouter = mainRouter
        self.session    = session
        
        storyboard = UIStoryboard(name: "Quiz", bundle: nil)
    }

    public func showCreateQuizFrom(vc: UIViewController) {

        let createQuizVC = storyboard.instantiateViewController(withIdentifier: String(describing: CreateQuizVC.self)) as! CreateQuizVC

        createQuizVC.router  = mainRouter
        createQuizVC.session = session

        vc.navigationController?.pushViewController(createQuizVC, animated: true)
        
        mainRouter.currentVC = vc
    }

    public func backFrom(quizVC: CreateQuizVC) {

        let navVC = quizVC.navigationController
        navVC?.popViewController(animated: true)
        
        navVC?.popViewController(animated: true)
        
        mainRouter.currentVC = navVC?.viewControllers.last
    }
    
    public func showTagSelectionFrom(parentVC: UIViewController, didFinishSelectionTags:(([DbTag]) -> Void)?) {
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: TagsSelectionVC.self)) as! TagsSelectionVC
        
        vc.session = session
        vc.router  = mainRouter
        vc.didFinishSelectionTags = didFinishSelectionTags

        parentVC.navigationController?.pushViewController(vc, animated: true)
        
        mainRouter.currentVC = vc
    }
    
    public func backFrom(tagsSelectionVC: TagsSelectionVC) {

        let navVC = tagsSelectionVC.navigationController
        
        navVC?.popViewController(animated: true)
        
        mainRouter.currentVC = navVC?.viewControllers.last
    }
}
