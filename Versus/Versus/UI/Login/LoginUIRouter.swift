//
//  LoginUIRouter.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/22/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation
import UIKit

class LoginUIRouter {
    
    private weak var mainRouter :UIRouter!
    private var session         :Session!
    private let storyboard      :UIStoryboard!
    
    init (mainRouter :UIRouter, session :Session) {
        
        self.mainRouter = mainRouter
        self.session    = session
        
        storyboard = UIStoryboard(name: "Login", bundle: nil)
    }

    public func loginVC() -> LoginVC {

        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: LoginVC.self)) as! LoginVC

        vc.router = mainRouter

        return vc
    }
}
