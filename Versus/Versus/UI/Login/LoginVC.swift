//
//  LoginVC.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/22/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit
import FirebaseUI

class LoginVC: UIViewController, FUIAuthDelegate {

    private var authUI  : FUIAuth!
    private var authListener: AuthStateDidChangeListenerHandle!
    override func viewDidLoad() {

        super.viewDidLoad()

        authUI = FUIAuth.defaultAuthUI()
        
        authUI?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        Auth.auth().removeStateDidChangeListener(authListener)
    }

    private var loginGoes = false
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        authListener = Auth.auth().addStateDidChangeListener {[weak self] (auth, user) in
            
            guard let _ = user else {
                self?.makeLogin()

                return
            }
            
            self?.router.showStartUpScreen()
//            print(user?.displayName)
//
//            user?.getIDToken(completion: { (token, error) in
//
//                print(token)
//            })
        }

       
    }
    
    private func makeLogin() {

        if !loginGoes {
            let providers: [FUIAuthProvider] = [
                FUIGoogleAuth(),
                FUIFacebookAuth(),
                ]
            
            authUI?.providers = providers
            
            present(authUI!.authViewController(), animated: false, completion: nil)
            
            loginGoes = true
        }
    }
    
    @IBAction func signOut(_ sender: Any) {

        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        
        if let error_ = error {
            
            ///TODO:- Handle error

        }else {
            
            router.showStartUpScreen()
        }
//        print(authDataResult)
//
//        print(authDataResult?.user)
//        print(authDataResult?.additionalUserInfo)
//        print(error)

       //
    }
}
