//
//  UIColor+VE.swift
//  Versus
//
//  Created by Eugene Liashenko on 6/19/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func vePurple() -> UIColor {
        
        return UIColor(displayP3Red: 139.0 / 256.0, green: 28.0 / 256.0, blue: 212.0 / 256.0, alpha: 1)
    }
}
