//
//  DateFormatter+VE.swift
//  Versus
//
//  Created by Eugene Liashenko on 6/5/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static let uiShort: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM dd HH:mm"//.SSSZZZZZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}
