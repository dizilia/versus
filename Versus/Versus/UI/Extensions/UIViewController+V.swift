//
//  UIViewController+V.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/22/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    private static var _myComputedProperty = [String:UIRouter]()
    
    var router:UIRouter {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UIViewController._myComputedProperty[tmpAddress]!
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UIViewController._myComputedProperty[tmpAddress] = newValue
        }
    }
}
