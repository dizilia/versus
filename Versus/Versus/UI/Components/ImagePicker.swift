//
//  ImagePicker.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/24/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import AVFoundation

class ImagePicker :NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private var picker :UIImagePickerController!
    
    private var holderVC :UIViewController!
    private var callback :((UIImage?)->Void)?

    init(holderVC :UIViewController) {

        super.init()

        self.holderVC = holderVC

        picker = UIImagePickerController()

        picker.delegate = self
    }

    public func selectPhoto(callback:((UIImage?)->Void)?) {

        self.callback = callback

        picker.allowsEditing = false
        picker.sourceType    = .photoLibrary

        holderVC.present(picker, animated: true, completion: nil)
    }
    
    public func takePhoto(callback:((UIImage?)->Void)?) {
        
        self.callback = callback
        
        cameraAsscessRequest {[weak self] granted in
            
            guard let self_ = self else { return }
            
            if !granted {
                
                callback?(nil)
                
                return
            }
            
            self_.picker.allowsEditing          = false
            self_.picker.sourceType             = UIImagePickerControllerSourceType.camera
            self_.picker.cameraCaptureMode      = .photo
            self_.picker.modalPresentationStyle = .fullScreen
            
            self_.holderVC.present(self_.picker,animated: true,completion: nil)
        }
    }
    
    func cameraAsscessRequest(asscessCallBack :((Bool)->Void)?) {
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            
            asscessCallBack?(true)
            
        } else {
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted -> Void in
                
                asscessCallBack?(granted)
            }
        }
    }

    //MARK: - Delegates
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        
        callback?(nil)
    }
    
    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        callback?(info[UIImagePickerControllerOriginalImage] as? UIImage)
    }
}
