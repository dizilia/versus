//
//  FeedVC.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/10/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit
import Firebase

class FeedVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var questions  : [DbQuestion] = []
    private var userAmswers: [DbAnswer] = []
    
    public var session: Session!

    override func viewDidLoad() {
        
        super.viewDidLoad()

        tableView.tableFooterView = UIView()

        session.dataProvider.didUpdateQuestions = {[weak self] questions in

            self?.session.dataProvider.getUsersAnswers(completion: {[weak self] answers in

                self?.applyNew(questions: questions, answers: answers)
            })
        }
        
        session.dataProvider.startListeners()        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = attributes
    }
    
    private func applyNew(questions:  [DbQuestion], answers:[DbAnswer]) {
        
        let needTableReload = self.questions.count != questions.count

        self.questions   = questions
        self.userAmswers = answers

        if needTableReload {

            tableView.reloadData()
        }
    }

    private func selectionStateFor(question: DbQuestion) -> FeedVariantsView.SelectionState {
        
        var selectionState :FeedVariantsView.SelectionState = .none
        
        if let answer = userAmswers.first(where: { (dbAnswer) -> Bool in

            return dbAnswer.questionId == question.id
        }) {
            
            selectionState = FeedVariantsView.SelectionState(rawValue: answer.answer) ?? .none
        }

        return selectionState
    }
    
    //MARK:- Table stuff
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedCell.self))! as! FeedCell

        let selectionState = selectionStateFor(question: questions[indexPath.row])
        
        cell.storage = session.dataProvider.storage

        cell.reloadwith(question: questions[indexPath.row], selection: selectionState, onVariant1Selected: {[unowned self] in

            self.updateVotesFor(question: self.questions[indexPath.row], firstSelected: true, secondSelected: false)

          //  self.tableView.reloadRows(at: [indexPath], with: .automatic)
            
        }) {
            
            self.updateVotesFor(question: self.questions[indexPath.row], firstSelected: false, secondSelected: true)
            
         //   self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        //cell.reload()

        
        
        return cell
    }
    
    private func updateVotesFor(question: DbQuestion, firstSelected: Bool, secondSelected: Bool) {
        
        let answer = firstSelected ? 1 : (secondSelected ? 2 : 0)
        
        session.dataProvider.voteFor(question: question, answer: answer)
    }
}
