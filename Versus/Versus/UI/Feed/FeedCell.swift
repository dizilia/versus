//
//  FeedCell.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/10/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit

class FeedCell :UITableViewCell {
    
    @IBOutlet weak var avaImageView     : UIImageView!
    @IBOutlet weak var nameLabel        : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
    @IBOutlet weak var statusPL         : UIView!
    @IBOutlet weak var dateLabel        : UILabel!
    @IBOutlet weak var variantsView     : FeedVariantsView!
    @IBOutlet weak var variantsHeight   : NSLayoutConstraint!
    @IBOutlet weak var bottomFirstLabel : UILabel!
    @IBOutlet weak var bottomSecondLabel: UILabel!

    public var onVariant1Selected:(()->Void)?
    public var onVariant2Selected:(()->Void)?

    public var storage :StorageDataProvider?
    
    private var question : DbQuestion!
    private var selection: FeedVariantsView.SelectionState!
    private var firstVotes : Int!
    private var secondVotes: Int!
    
    public func reloadwith(question: DbQuestion, selection: FeedVariantsView.SelectionState, onVariant1Selected:(()->Void)?, onVariant2Selected:(()->Void)?) {

        self.question  = question
        self.selection = selection
        firstVotes  = question.voteOption1
        secondVotes = question.voteOption2

        avaImageView.image = nil
        variantsView.variant1ImageView.image = nil
        variantsView.variant2ImageView.image = nil
        
        nameLabel.text = question.creatorName
        statusPL.isHidden = true

        variantsHeight.constant = variantsView.apply(state: question.isVertical ? .vertical : .horizintal)
        
        variantsView.reloadWith(selection: selection)

        variantsView.onVariant1Selected = {[unowned self] in
          
            if self.selection == .none {
                
                self.firstVotes = self.firstVotes + 1
                
            }else if self.selection == .second {

                self.firstVotes  = self.firstVotes + 1
                self.secondVotes = self.secondVotes - 1
            }
            
            self.selection = .first
            
            self.updateVotesValues()
            
            onVariant1Selected?()
        }
        
        variantsView.onVariant2Selected = { [unowned self] in
          
            if self.selection == .none {

                self.secondVotes = self.secondVotes + 1
                
            }else if self.selection == .first {
                
                self.firstVotes  = self.firstVotes - 1
                self.secondVotes = self.secondVotes + 1
            }

            self.selection = .second

            self.updateVotesValues()
            
            onVariant2Selected?()
        }
        
        updateVotesValues()
        
        descriptionLabel.text = question.description.isEmpty ? question.title : question.description

        dateLabel.text = DateFormatter.uiShort.string(from: question.creationDate())

        storage?.getImage(named: question.image1name, completion: {[weak self] image in
            
            guard let self_ = self, self_.question.id == question.id else {
                return
            }

            self_.variantsView.variant1ImageView.image = image
        })
        
        storage?.getImage(named: question.image2name, completion: {[weak self] image in
            
            guard let self_ = self, self_.question.id == question.id else {
                return
            }

            self?.variantsView.variant2ImageView.image = image

        })
        
        storage?.getImage(named: question.creatorAvatar, completion: { [weak self] image in

            guard let self_ = self, self_.question.id == question.id else {
                return
            }

            self?.avaImageView.image = image
        })
    }
    
    private func updateVotesValues() {
        
        bottomFirstLabel.text  = "\(firstVotes!) votes"
        bottomSecondLabel.text = "\(secondVotes!) votes"
    }
}
