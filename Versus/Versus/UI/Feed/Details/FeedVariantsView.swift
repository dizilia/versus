//
//  FeedVariantsView.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/10/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit

class FeedVariantsView :UIView {
    
    enum State {
        case horizintal
        case vertical
    }
    
    enum SelectionState :Int{
        case none
        case first
        case second
    }

    @IBOutlet weak var variant1PlView    : UIView!
    @IBOutlet weak var variant2PlView    : UIView!
    @IBOutlet weak var variant1ImageView : UIImageView!
    @IBOutlet weak var variant2ImageView : UIImageView!
    
    private var state :State = .horizintal
    private var selectionState: SelectionState = .none
    
    public var onVariant1Selected:(()->Void)?
    public var onVariant2Selected:(()->Void)?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        variant1PlView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectVariant1(_:))))
        variant2PlView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectVariant2(_:))))
        
        variant1PlView.layer.borderWidth = 1
        variant1PlView.layer.borderColor = UIColor.white.cgColor
        
        variant2PlView.layer.borderWidth = 1
        variant2PlView.layer.borderColor = UIColor.white.cgColor
    }
    
    @objc public func selectVariant1(_ gesture: UITapGestureRecognizer) {
        
        apply(newSelectionState: .first, animated: true) {[weak self] in
            
            self?.onVariant1Selected?()
        }
    }

    @objc public func selectVariant2(_ gesture: UITapGestureRecognizer) {
        
        apply(newSelectionState: .second, animated: true) {[weak self] in
            
            self?.onVariant2Selected?()
        }
    }
    
    private var v1Leading :NSLayoutConstraint!
    private var v1Trailing:NSLayoutConstraint!
    private var v1Top     :NSLayoutConstraint!
    private var v1Bottom  :NSLayoutConstraint!
    private var v1Width   :NSLayoutConstraint!
    private var v1Height  :NSLayoutConstraint!
    
    private var v2Leading :NSLayoutConstraint!
    private var v2Trailing:NSLayoutConstraint!
    private var v2Top     :NSLayoutConstraint!
    private var v2Bottom  :NSLayoutConstraint!
    private var v2Width   :NSLayoutConstraint!
    private var v2Height  :NSLayoutConstraint!

    private var isSetuped = false
    
    public func apply(state: State)  -> CGFloat {
        
        let result :CGFloat = state == .horizintal ? 300 : 200
        
        if isSetuped && state == self.state {

            return result
        }

        isSetuped = true

        variant1PlView.translatesAutoresizingMaskIntoConstraints = false
        variant2PlView.translatesAutoresizingMaskIntoConstraints = false
        
        variant1PlView.deactivateAllConstraints()
        variant2PlView.deactivateAllConstraints()
        
        if state == .horizintal {
            
            v1Leading = variant1PlView.leadingAnchor.constraint(equalTo: leadingAnchor)
            v1Leading.isActive = true
            
            v1Top = variant1PlView.topAnchor.constraint(equalTo: topAnchor)
            v1Top.isActive = true
            
            v1Bottom = variant1PlView.bottomAnchor.constraint(equalTo: bottomAnchor)
            v1Bottom.isActive = true
            
            v1Width = variant1PlView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5)
            v1Width.isActive = true
            
            v2Trailing = variant2PlView.trailingAnchor.constraint(equalTo: trailingAnchor)
            v2Trailing.isActive = true
            
            v2Top = variant2PlView.topAnchor.constraint(equalTo: topAnchor)
            v2Top.isActive = true
            
            
            v2Bottom = variant2PlView.bottomAnchor.constraint(equalTo: bottomAnchor)
            v2Bottom.isActive = true
            
            v2Width = variant2PlView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5)
            v2Width.isActive = true
            
        }else {
            
            v1Leading = variant1PlView.leadingAnchor.constraint(equalTo: leadingAnchor)
            v1Leading.isActive = true
            
            v1Trailing = variant1PlView.trailingAnchor.constraint(equalTo: trailingAnchor)
            v1Trailing.isActive = true
            
            v1Top = variant1PlView.topAnchor.constraint(equalTo: topAnchor)
            v1Top.isActive = true
            
            v1Height = variant1PlView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5)
            v1Height.isActive = true
            
            v2Leading = variant2PlView.leadingAnchor.constraint(equalTo: leadingAnchor)
            v2Leading.isActive = true
            
            v2Trailing = variant2PlView.trailingAnchor.constraint(equalTo: trailingAnchor)
            v2Trailing.isActive = true
            
            v2Bottom = variant2PlView.bottomAnchor.constraint(equalTo: bottomAnchor)
            v2Bottom.isActive = true
            
            v2Height = variant2PlView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5)
            v2Height.isActive = true
        }
        
        selectionState = .none
        
        return result
    }
    
    public func reloadWith(selection: SelectionState = .none) {

        apply(newSelectionState: selection, animated: false)
    }

    private func apply(newSelectionState: SelectionState, animated: Bool, completion:(()->Void)? = nil) {
        
        let animationSpeed = 0.1
        
        let perform :((Bool, (()->Void)?)->Void) = { (animated, compl) in
            
            self.setNeedsUpdateConstraints()
                
            UIView.animate(withDuration: animated ?  animationSpeed : 0, delay: 0, options: .curveLinear, animations: {
                    
                    self.layoutIfNeeded()
                    
                }) { (b) in
                    
                    compl?()
                }
          
        }
        
        if selectionState == newSelectionState {

            completion?()
            return
        }
        
        applyNoneVariantConstrains()
        
        perform(animated) { [weak self] in
            
            guard let self_ = self else {
                return
            }
            
            if self_.state == .horizintal && newSelectionState == .first {
                
                self_.applyFirstVariantConstrains()
                
                perform(animated, completion)
                
            }
            
            if self_.state == .horizintal && newSelectionState == .second {
                
                self_.applySecondVariantConstrains()
                
                perform(animated, completion)
            }
            
            self_.selectionState = newSelectionState
        }
    }
    
    private let offset :CGFloat = 10
    
    func applyNoneVariantConstrains() {
        
        if state == .horizintal && selectionState == .first {
            
            v1Top.constant     += offset
            v1Leading.constant += offset
            v1Bottom.constant  -= offset
            v1Width.constant   -= (offset * 5)
            
            v2Trailing.constant += offset
            v2Width.constant    += offset
            v2Top.constant      -= offset
            v2Bottom.constant   += offset
        }
        
        if state == .horizintal && selectionState == .second {
            
            v1Top.constant     -= offset
            v1Leading.constant -= offset
            v1Bottom.constant  += offset
            v1Width.constant   += offset
            
            v2Trailing.constant -= offset
            v2Width.constant    -= (offset * 5)
            v2Top.constant      += offset
            v2Bottom.constant   -= offset
        }
        variant1PlView.alpha = 1
        variant2PlView.alpha = 1
    }
    
    func applyFirstVariantConstrains() {
        
        v1Top.constant     -= offset
        v1Bottom.constant  += offset
        v1Leading.constant -= offset
        v1Width.constant   += (offset * 5)
        
        v2Trailing.constant -= offset
        v2Width.constant    -= offset
        v2Top.constant      += offset
        v2Bottom.constant   -= offset

        variant1PlView.alpha = 1
        variant2PlView.alpha = 0.5
        
        bringSubview(toFront: variant1PlView)
    }

    func applySecondVariantConstrains() {

        v1Top.constant     += offset
        v1Leading.constant += offset
        v1Bottom.constant  -= offset
        v1Width.constant   -= offset
        
        v2Trailing.constant += offset
        v2Width.constant    += (offset * 5)
        v2Top.constant      -= offset
        v2Bottom.constant   += offset
        
        variant1PlView.alpha = 0.5
        variant2PlView.alpha = 1.0
        
        bringSubview(toFront: variant2PlView)
    }
}
