//
//  UIRouter.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/10/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit

class UIRouter {
    
    private var window    :UIWindow!
    public  var currentVC :UIViewController?
    private var session   :Session
    
    init(appDelegat :AppDelegate, session :Session) {

        self.session = session
        
        appDelegat.window = UIWindow(frame :UIScreen.main.bounds)
        
        window = appDelegat.window
    }
    
    public func showLoginScreen() {

        window.rootViewController = loginUIRouter.loginVC()

        window.makeKeyAndVisible()

        currentVC = window.rootViewController
    }
    
    public func showStartUpScreen() {
        
        window.rootViewController = homeUIRouter.homeContainer()
        //Config.loadMainScreenOnStart
        //            ? mainUIRouter.mainViewController()
        //            : onboardUIRouter.startUpNavigationController()
        
        window.makeKeyAndVisible()
        
        currentVC = window.rootViewController
    }
    
    public func show(message: String) {
        
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        currentVC?.present(alert, animated: true, completion: nil)
    }

    public lazy var homeUIRouter: HomeUIRouter = {

        return HomeUIRouter(mainRouter: self, session: session)
    }()
    
    public lazy var loginUIRouter: LoginUIRouter = {

        return LoginUIRouter(mainRouter: self, session: session)
    }()

    public lazy var quizUIRouter: QuizUIRouter = {

        return QuizUIRouter(mainRouter: self, session: session)
    }()    
}
