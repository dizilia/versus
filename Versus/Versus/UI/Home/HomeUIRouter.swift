//
//  HomeUIRouter.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/10/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import Foundation
import UIKit

class HomeUIRouter {
    
    private weak var mainRouter :UIRouter!
    private var session         :Session!
    private let storyboard      :UIStoryboard!
    
    init (mainRouter :UIRouter, session :Session) {

        self.mainRouter = mainRouter
        self.session    = session

        storyboard = UIStoryboard(name: "Main", bundle: nil)
    }
    
    public func homeContainer() -> UIViewController {
        
        let navVC = storyboard.instantiateViewController(withIdentifier: String(describing: "HomeNavVC")) as! UINavigationController
        
        let homeVC = navVC.viewControllers.first as! HomeVC
        
        homeVC.router  = mainRouter
        homeVC.session = session

        return navVC
    }
}
