//
//  HomeVC.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/10/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
 
    public var session :Session!
    
    private var feedVC :FeedVC!
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? FeedVC {

            feedVC = vc
            
            feedVC.session = session
        }
    }
    
    @IBAction func createQuiz(_ sender: Any) {

        router.quizUIRouter.showCreateQuizFrom(vc: self)
    }
}
