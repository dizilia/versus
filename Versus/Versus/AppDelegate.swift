//
//  AppDelegate.swift
//  Versus
//
//  Created by Eugene Liashenko on 5/8/18.
//  Copyright © 2018 pepper_salt. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private var uiRouter: UIRouter!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        UIApplication.shared.statusBarStyle = .lightContent

        FirebaseApp.configure()
        
        let session = Session()
        
        uiRouter = UIRouter(appDelegat: self, session: session)

        uiRouter.showLoginScreen()

        let link = CADisplayLink(target: self, selector: #selector(AppDelegate.update(link:)))
        link.add(to: RunLoop.main, forMode: .commonModes)
        
        return true
    }

    private var lastTime  :Double = 0.0
    private var firstTime :Double = 0.0
    @objc func update(link: CADisplayLink) {
        
        if lastTime == 0.0 {
            firstTime = link.timestamp
            lastTime  = link.timestamp
        }
        
        let currentTime = link.timestamp
        let elapsedTime = floor((currentTime - lastTime) * 10_000) / 10
        let totalElapsedTime = currentTime - firstTime
        
        if elapsedTime > 16.7 {
            print("Frame was dropped with elapsed time of \(elapsedTime) at \(totalElapsedTime)")
        }
        
        lastTime = link.timestamp
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        
        let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?
        
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {

            return true
        }

        return false
    }
}

extension AppDelegate {

    public func router() -> UIRouter {

        return uiRouter
    }
}

